package com.touch.self.sismgnt.dao;

import com.touch.self.sismgnt.model.StudentType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentTypeRepository extends JpaRepository<StudentType,Integer> {
}
