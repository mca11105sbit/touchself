package com.touch.self.sismgnt.controller;

import com.touch.self.sismgnt.dao.StudentTypeRepository;
import com.touch.self.sismgnt.model.StudentCategory;
import com.touch.self.sismgnt.model.StudentType;
import com.touch.self.sismgnt.service.IStudentCategory;
import com.touch.self.sismgnt.service.IStudentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("sis")
public class SISMgnt {


    @Autowired
    IStudentType iStudentType;

    @Autowired
    IStudentCategory iStudentCategory;

    @GetMapping("studenttype/get/{id}")
    public ResponseEntity<StudentType> getStudentType(@PathVariable Integer id) {
        return ResponseEntity.ok(iStudentType.get(id));
    }

    @GetMapping("studenttype/getAll")
    public ResponseEntity<List<StudentType>> getStudentTypeAll() {
        return ResponseEntity.ok(iStudentType.getAll());
    }

    //@CrossOrigin
    @PostMapping("studenttype/save")
    public ResponseEntity<StudentType> saveStudentType(@RequestBody StudentType studentType) {
        return ResponseEntity.ok(iStudentType.save(studentType));
    }

    @GetMapping("studenttype/delete/{id}")
    public ResponseEntity<String> deleteStudentType(@PathVariable Integer id) {
        iStudentType.delete(id);
        return ResponseEntity.ok("Successfully Deleted");
    }



    @GetMapping("studentcategory/get/{id}")
    public ResponseEntity<StudentCategory> getStudentCategory(@PathVariable Integer id) {
        return ResponseEntity.ok(iStudentCategory.get(id));
    }

    @GetMapping("studentcategory/getAll")
    public ResponseEntity<List<StudentCategory>> getStudentCategoryAll() {
        return ResponseEntity.ok(iStudentCategory.getAll());
    }

    //@CrossOrigin
    @PostMapping("studentcategory/save")
    public ResponseEntity<StudentCategory> saveStudentCategory(@RequestBody StudentCategory studentCategory) {
        return ResponseEntity.ok(iStudentCategory.save(studentCategory));
    }

    @GetMapping("studentcategory/delete/{id}")
    public ResponseEntity<String> deleteStudentCategory(@PathVariable Integer id) {
        iStudentType.delete(id);
        return ResponseEntity.ok("Successfully Deleted");
    }

}

