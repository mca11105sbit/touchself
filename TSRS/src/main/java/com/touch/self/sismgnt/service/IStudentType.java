package com.touch.self.sismgnt.service;

import com.touch.self.sismgnt.model.StudentType;

import java.util.List;

public interface IStudentType {

    public StudentType get(Integer id);

    public List<StudentType> getAll();

    public StudentType save(StudentType studentType);

    public String delete(Integer id);
}
