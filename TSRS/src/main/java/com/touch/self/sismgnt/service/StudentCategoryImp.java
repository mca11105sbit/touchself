package com.touch.self.sismgnt.service;

import com.touch.self.sismgnt.dao.StudentCategoryRepository;
import com.touch.self.sismgnt.model.StudentCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentCategoryImp implements IStudentCategory {
    @Autowired
    StudentCategoryRepository studentCategoryRepository;

    @Override
    public StudentCategory get(Integer id) {
        return studentCategoryRepository.findById(id).get();
    }

    @Override
    public List<StudentCategory> getAll() {
        return studentCategoryRepository.findAll();
    }

    @Override
    public StudentCategory save(StudentCategory studentType) {
        return studentCategoryRepository.save(studentType);
    }

    @Override
    public String delete(Integer id) {
        studentCategoryRepository.deleteById(id);
        return "Successfull";
    }
}
