package com.touch.self.sismgnt.dao;

import com.touch.self.sismgnt.model.StudentCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentCategoryRepository extends JpaRepository<StudentCategory,Integer> {
}
