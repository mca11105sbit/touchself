package com.touch.self.sismgnt.service;

import com.touch.self.sismgnt.model.StudentCategory;
import com.touch.self.sismgnt.model.StudentType;

import java.util.List;

public interface IStudentCategory {

    public StudentCategory get(Integer id);

    public List<StudentCategory> getAll();

    public StudentCategory save(StudentCategory studentCategory);

    public String delete(Integer id);
}
