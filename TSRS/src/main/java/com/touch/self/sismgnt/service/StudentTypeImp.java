package com.touch.self.sismgnt.service;

import com.touch.self.sismgnt.dao.StudentTypeRepository;
import com.touch.self.sismgnt.model.StudentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentTypeImp implements IStudentType {

    @Autowired
    StudentTypeRepository studentTypeRepository;

    @Override
    public StudentType get(Integer id) {
        return studentTypeRepository.findById(id).get();
    }

    @Override
    public List<StudentType> getAll() {
        return studentTypeRepository.findAll();
    }

    @Override
    public StudentType save(StudentType studentType) {
        return studentTypeRepository.save(studentType);
    }

    @Override
    public String delete(Integer id) {
        studentTypeRepository.deleteById(id);
        return "Successfully";
    }
}
