package com.touch.self.sismgnt.model;

import lombok.Data;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "studenttype")
@Data
@Component
public class StudentType {

    @Id
    @GeneratedValue
    Integer id;
    String name;
    Integer seq;

}
