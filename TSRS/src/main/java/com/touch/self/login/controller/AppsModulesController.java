package com.touch.self.login.controller;


import com.touch.self.login.model.AppsModules;
import com.touch.self.login.service.AppsModulesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AppsModulesController {

    @Autowired
    AppsModulesService appsModulesService;

    @GetMapping("Appsmodules")
    public ResponseEntity<List<AppsModules>> getAppsModules(){

        System.out.println("Apps Modules");

        return new ResponseEntity<List<AppsModules>>(appsModulesService.getAllAppsModule(), HttpStatus.OK);
    }
}
