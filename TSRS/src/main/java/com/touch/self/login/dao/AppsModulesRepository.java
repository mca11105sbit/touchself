package com.touch.self.login.dao;

import com.touch.self.login.model.AppsModules;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppsModulesRepository extends JpaRepository<AppsModules,Long> {

}
