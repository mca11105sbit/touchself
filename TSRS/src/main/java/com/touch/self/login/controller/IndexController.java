package com.touch.self.login.controller;

import com.touch.self.login.model.Organization;
import com.touch.self.login.service.IOrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class IndexController {

    @Autowired
    IOrganizationService iOrganizationService;

    @GetMapping("/Index")
    public ResponseEntity<Organization> showDataString(){
        System.out.println("hello  uttttt");
        System.out.println("hello 99");
        List<Organization> orglist= iOrganizationService.getAll();
        ResponseEntity<Organization> responseEntity=new ResponseEntity( orglist.size() > 0 ? orglist.get(0):new Organization(),HttpStatus.OK);

        return responseEntity ;
    }

}
