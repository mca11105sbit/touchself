package com.touch.self.login.service;

import com.touch.self.login.dao.AppsModulesRepository;
import com.touch.self.login.model.AppsModules;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppsModulesServiceImp implements AppsModulesService {

    @Autowired
    AppsModulesRepository appsModulesRepository;

    @Override
    public AppsModules getAppsModules() {
        return appsModulesRepository.getOne(0l);
    }

    @Override
    public List<AppsModules> getAllAppsModule() {
        return appsModulesRepository.findAll();
    }
}
