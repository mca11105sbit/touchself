package com.touch.self.login.service;

import com.touch.self.login.model.Organization;

import java.util.List;

public interface IOrganizationService {
    public Organization get(Long id);

    public List<Organization> getAll();
}
