package com.touch.self.login.model;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "module")
public class AppsModules {

    @Id
    @GeneratedValue
    Long id;
    String name;
    String description;
    String classname;
}
