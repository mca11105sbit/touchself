package com.touch.self.login.service;

import com.touch.self.login.dao.OrganizationRepository;
import com.touch.self.login.model.Organization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrganizationServiceImp  implements IOrganizationService {

    @Autowired
    OrganizationRepository organizationRepository;

    @Override
    public Organization get(Long id) {
        Optional<Organization>  org = organizationRepository.findById(id);
        //return org.isPresent() ?  org.get():org.orElse(new Organization()) ;
        return org.orElse(new Organization()) ;
    }

    @Override
    public List<Organization> getAll() {
        return organizationRepository.findAll() ;
    }
}
