package com.touch.self.login.model;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "organization")
//@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Organization {

  @Id
  @GeneratedValue
  Long  id;
  String name;
  String address;
  String mobileno;

}
