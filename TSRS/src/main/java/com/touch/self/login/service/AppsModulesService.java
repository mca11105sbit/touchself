package com.touch.self.login.service;

import com.touch.self.login.model.AppsModules;

import java.util.List;

public interface AppsModulesService {

    public AppsModules getAppsModules();

    public List<AppsModules> getAllAppsModule();

}
