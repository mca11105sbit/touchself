package com.touch.self.config.service;

import com.touch.self.config.model.ViewTab;

import java.util.List;

public interface IViewTab {
    public ViewTab get(Integer id);

    public List<ViewTab> getAll();

    public ViewTab save(ViewTab studentType);

    public String delete(Integer id);
}
