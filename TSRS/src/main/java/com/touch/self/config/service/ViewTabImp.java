package com.touch.self.config.service;

import com.touch.self.config.dao.ViewTabRepository;
import com.touch.self.config.model.ViewTab;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ViewTabImp implements  IViewTab {

    @Autowired
    ViewTabRepository viewTabRepository;

    @Override
    public ViewTab get(Integer id) {
        return viewTabRepository.findById(id).get();
    }

    @Override
    public List<ViewTab> getAll() {
        return viewTabRepository.findAll();
    }

    @Override
    public ViewTab save(ViewTab viewTab) {
        return viewTabRepository.save(viewTab);
    }

    @Override
    public String delete(Integer id) {
        viewTabRepository.deleteById(id);
        return "Successfully";
    }
}
