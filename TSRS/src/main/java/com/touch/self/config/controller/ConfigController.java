package com.touch.self.config.controller;

import com.touch.self.config.model.ViewTab;
import com.touch.self.config.service.IViewTab;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("Config")
public class ConfigController {

    @Autowired
    IViewTab iViewTab;

    @GetMapping("ViewTab")
    public ResponseEntity<List<ViewTab>> getViewTabAll() {
        return ResponseEntity.ok(iViewTab.getAll());
    }


    @GetMapping("ViewTab/{id}")
    public ResponseEntity<ViewTab> getViewTab(@PathVariable Integer id) {
        return ResponseEntity.ok(iViewTab.get(id));
    }


    @PostMapping("ViewTab")
    public ResponseEntity<ViewTab> saveViewTab(@RequestBody ViewTab viewTab) {
        return ResponseEntity.ok(iViewTab.save(viewTab));
    }

    @PutMapping("ViewTab")
    public ResponseEntity<ViewTab> updateViewTab(@RequestBody ViewTab viewTab) {
        return ResponseEntity.ok(iViewTab.save(viewTab));
    }

    @DeleteMapping("ViewTab/{id}")
    public ResponseEntity<String> deleteViewTab(@PathVariable Integer id) {
        iViewTab.delete(id);
        return ResponseEntity.ok("Successfully Deleted");
    }

}
