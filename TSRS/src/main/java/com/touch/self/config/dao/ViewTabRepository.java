package com.touch.self.config.dao;

import com.touch.self.config.model.ViewTab;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ViewTabRepository extends JpaRepository<ViewTab,Integer> {
}
