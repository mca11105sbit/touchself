package com.touch.self.mypack;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Test {

    @GetMapping("/Hello")
    public ResponseEntity<String> showDataString(){
        System.out.println("hello ");
        System.out.println("hello 99");
        ResponseEntity<String> responseEntity=new ResponseEntity("hi", HttpStatus.OK);

        return responseEntity ;
    }
}

