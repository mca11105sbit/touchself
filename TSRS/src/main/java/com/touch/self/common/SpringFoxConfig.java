package com.touch.self.common;

import static springfox.documentation.builders.PathSelectors.regex;
import static springfox.documentation.builders.RequestHandlerSelectors.basePackage;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


import java.util.Collections;

@Configuration
@EnableSwagger2
public class SpringFoxConfig {

    @Bean
    public Docket createDocket() {
        return new Docket(DocumentationType.SWAGGER_2) // Swagger Screen
                .select() // read rest controllers
                .apis(basePackage("com.touch.self")) // (api)rest controller from common package name
                .paths(regex("/*.*")) // having one common starting path (/.*)
                //.paths(regex("/sis.*")) // having one common starting path (/.*)
                .build() //create screen
                .useDefaultResponseMessages(false) // hide error codes at swagger
                .apiInfo(apiInfo()) //optional meta data
                ;

    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Product TSI APP",
                "This is a sample Product Store",
                "1.2 GA",
                "https://nareshit.in/",
                new Contact("Rock", "http://touchself.in/", "manjeetoberoy@gmail.com"),
                "TSI Lmtd License",
                "https://touchself.in/license",
                Collections.emptyList());
    }
}
