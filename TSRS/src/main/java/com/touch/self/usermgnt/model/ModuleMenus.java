package com.touch.self.usermgnt.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "modulemenus")
public class ModuleMenus {
    @Id
    @GeneratedValue
    Long id;
    Long appsModuleId;
    Long menuid;
    Long seq;
    String path;
    String className;
    String icon;


}
